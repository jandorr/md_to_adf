const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const fnTranslate = require('md-to-adf');


const port = process.env.PORT || 8080
const app = express()


app.use(bodyParser.urlencoded({extended: true})); 

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'))
    // res.send("A minimal web app to convert Markdown to Atlassian Document Format.\n\nPOST /md-to-adf - Converts Mardown to Atlassian Document Format (use the body parameter 'markdown')")
})

app.post('/md-to-adf', (req, res) => {
    adf = fnTranslate(req.body.markdown)
    adf_json = JSON.stringify(adf, null, 4)
    res.send(adf_json)
})

app.listen(port, () => console.log(`App listening on port ${port}`))
